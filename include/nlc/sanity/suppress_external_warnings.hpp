/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

// clang-format off

// MSVC (and not clang-cl !!!)
#if defined(_MSC_VER) && !defined(__clang__)

#   define BEGIN_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE _Pragma("warning(push, 0)")
#   define END_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE _Pragma("warning(pop)")

#elif defined(__GNUC__) || defined(__clang__)

#   define BEGIN_GNU_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE \
        _Pragma("GCC diagnostic push") \
        _Pragma("GCC diagnostic ignored \"-Wcast-align\"") \
        _Pragma("GCC diagnostic ignored \"-Wcast-qual\"") \
        _Pragma("GCC diagnostic ignored \"-Wconversion\"") \
        _Pragma("GCC diagnostic ignored \"-Wdouble-promotion\"") \
        _Pragma("GCC diagnostic ignored \"-Wimplicit-fallthrough\"") \
        _Pragma("GCC diagnostic ignored \"-Wold-style-cast\"") \
        _Pragma("GCC diagnostic ignored \"-Wsign-compare\"") \
        _Pragma("GCC diagnostic ignored \"-Wsign-conversion\"") \
        _Pragma("GCC diagnostic ignored \"-Wswitch-enum\"")\
        _Pragma("GCC diagnostic ignored \"-Wunused-function\"") \
        _Pragma("GCC diagnostic ignored \"-Wunused-macros\"") \
        _Pragma("GCC diagnostic ignored \"-Wzero-as-null-pointer-constant\"") \
        _Pragma("GCC diagnostic ignored \"-Wmissing-field-initializers\"")
// Clang
#   if defined(__clang__)
#       define BEGIN_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE \
            BEGIN_GNU_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE \
            _Pragma("GCC diagnostic ignored \"-Wimplicit-int-conversion\"") \
            _Pragma("GCC diagnostic ignored \"-Wimplicit-int-float-conversion\"") \
            _Pragma("GCC diagnostic ignored \"-Wextra-semi-stmt\"") \
            _Pragma("GCC diagnostic ignored \"-Wenum-enum-conversion\"") \
            _Pragma("GCC diagnostic ignored \"-Wreserved-id-macro\"") \
            _Pragma("GCC diagnostic ignored \"-Wnonportable-system-include-path\"") \
            _Pragma("GCC diagnostic ignored \"-Wused-but-marked-unused\"") \
            _Pragma("GCC diagnostic ignored \"-Wdisabled-macro-expansion\"") \
            _Pragma("GCC diagnostic ignored \"-Wmissing-prototypes\"") \
            _Pragma("GCC diagnostic ignored \"-Wdeprecated-volatile\"") /* C++20 */
// GCC
#   elif defined(__GNUC__)
#       define BEGIN_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE \
            BEGIN_GNU_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE \
            _Pragma("GCC diagnostic ignored \"-Wformat=\"") \
            _Pragma("GCC diagnostic ignored \"-Wduplicated-branches\"") \
            _Pragma("GCC diagnostic ignored \"-Walloc-zero\"") \
            _Pragma("GCC diagnostic ignored \"-Wuseless-cast\"")\
            _Pragma("GCC diagnostic ignored \"-Wunused-but-set-variable\"") \
            _Pragma("GCC diagnostic ignored \"-Wtype-limits\"") \
            _Pragma("GCC diagnostic ignored \"-Wmissing-declarations\"") \
            _Pragma("GCC diagnostic ignored \"-Wvolatile\"") /* C++20 */ \
            _Pragma("GCC diagnostic ignored \"-Wnull-dereference\"")

#   endif
#   define END_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE _Pragma("GCC diagnostic pop")

// Other
#else

#   define BEGIN_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE
#   define END_SUPPRESS_WARNINGS_FOR_EXTERNAL_CODE

#endif
